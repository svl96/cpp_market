#include <initializer_list>
#include <string>
#include <vector>
#include "../entity/entities.h"

#pragma once

enum DOCFIELD {TITLE, PRICE, DISCOUNT, LATITUDE, LONGITUDE, SHOP};

class ISerializer {
public:
    ISerializer() = default;

    ISerializer(std::initializer_list<DOCFIELD> fields);

    virtual ~ISerializer() = default;

    virtual std::string Serialize(const Document &doc) const = 0;

    virtual std::string Serialize(const Document &doc, char separator, bool highlights) const;

protected:
    std::vector<DOCFIELD> fields_;

};

class CSVSerializer : public ISerializer {
public:
    CSVSerializer(std::initializer_list<DOCFIELD> fields);

    ~CSVSerializer() override = default;

    std::string Serialize(const Document& doc) const override;

};

class TSVSerializer : public ISerializer {
public:
    TSVSerializer(std::initializer_list<DOCFIELD> fields);

    ~TSVSerializer() override = default;

    std::string Serialize(const Document& doc) const override;

};

class HighlightTitleCSVSerializer : public ISerializer {
public:
    HighlightTitleCSVSerializer() = default;

    HighlightTitleCSVSerializer(std::initializer_list<DOCFIELD> fields);

    ~HighlightTitleCSVSerializer() override = default;

    std::string Serialize(const Document& doc) const override;

};