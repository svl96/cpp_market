
#include "filters.h"
#include "utils.h"

// region TitleFilter

TitleFilter::TitleFilter(const std::string &title) : title_(toLowerCase(title)) {}

bool TitleFilter::Check(const Document &doc) const {

    std::string insens_title = toLowerCase(doc.highlight_title);
    size_t index = insens_title.find(title_);
    if (index != std::string::npos) {
        std::string tmp = doc.highlight_title;
        tmp.insert(index + title_.size(), 1, '*');
        tmp.insert(index, 1, '*');
        doc.highlight_title = tmp;
    }

    return index != std::string::npos;
}

// endregion

// region PriceFilter

PriceFilter::PriceFilter(size_t max) : min_(0), max_(max) {}

PriceFilter::PriceFilter(size_t min, size_t max) : min_(min), max_(max) {}

bool PriceFilter::Check(const Document &doc) const {
    return min_ <= doc.price && doc.price <= max_;
}

// endregion

// region LocationFilter

LocationFilter::LocationFilter(double lat, double lng, double dist_km) :
    lat_(lat), lng_(lng), dist_km_(dist_km) {}

bool LocationFilter::Check(const Document &doc) const {
    double dist = GetDistBetweenPoints(lat_, lng_, doc.latitude, doc.longitude);
    return dist <= dist_km_;
}

// endregion

// region ShopFilter

ShopFilter::ShopFilter(const std::vector<std::string> &shops) : shops_(shops) {}

bool ShopFilter::Check(const Document &doc) const {

    for (const std::string &shop : shops_) {
        if (shop == doc.shop) {
            return true;
        }
    }

    return false;
}

// endregion

// region PriceWithDiscountFilter

PriceWithDiscountFilter::PriceWithDiscountFilter(size_t max)
            : min_(0), max_(max) {}

PriceWithDiscountFilter::PriceWithDiscountFilter(size_t min, size_t max)
            : min_(min), max_(max) {}

bool PriceWithDiscountFilter::Check(const Document &doc) const {
    double dics_price = static_cast<int>(doc.price * static_cast<double>(100 - doc.discount) / 100);
    return min_ <= dics_price && dics_price <= max_;
}

// endregion
