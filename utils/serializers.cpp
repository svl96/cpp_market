
#include "serializers.h"
#include "utils.h"

namespace {

std::string GetDocumentFieldValue(const Document doc, DOCFIELD field, bool highlight = false) {
    switch (field) {
        case TITLE:
            return highlight && !doc.highlight_title.empty() ? doc.highlight_title : doc.title;
        case PRICE:
            return std::to_string(doc.price);
        case DISCOUNT:
            return std::to_string(doc.discount);
        case LATITUDE:
            return DoubleToString(doc.latitude);
        case LONGITUDE:
            return DoubleToString(doc.longitude);
        case SHOP:
            return doc.shop;
        default:
            return "";
    }
}
}

std::string ISerializer::Serialize(const Document &doc, char separator, bool highlights) const {
    std::string result;
    for (DOCFIELD field : fields_) {
        result += GetDocumentFieldValue(doc, field, highlights) + separator;
    }
    if (!result.empty()) {
        result.pop_back();
    }
    return result;
}

ISerializer::ISerializer(std::initializer_list<DOCFIELD> fields) : fields_(fields) {}

CSVSerializer::CSVSerializer(std::initializer_list<DOCFIELD> fields)
       : ISerializer(fields) {}

std::string CSVSerializer::Serialize(const Document &doc) const {
    return ISerializer::Serialize(doc, ',', false);
}

TSVSerializer::TSVSerializer(std::initializer_list<DOCFIELD> fields)
        : ISerializer(fields) {}

std::string TSVSerializer::Serialize(const Document &doc) const {
    return ISerializer::Serialize(doc, '\t', false);
}

HighlightTitleCSVSerializer::HighlightTitleCSVSerializer(std::initializer_list<DOCFIELD> fields)
        : ISerializer(fields) {}

std::string HighlightTitleCSVSerializer::Serialize(const Document &doc) const {
    return ISerializer::Serialize(doc, ',', true);
}
