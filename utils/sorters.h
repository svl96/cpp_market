#pragma once

#include "../entity/entities.h"

class ISort {
public:
    ISort() = default;

    virtual ~ISort() = default;;

    virtual bool IsGreater(Document a, Document b) const = 0;

    virtual bool AreEqual(Document a, Document b) const = 0;
};

class SortByTitle : public ISort {
public:
    SortByTitle() = default;

    ~SortByTitle() override = default;

    bool IsGreater(Document a, Document b) const override;

    bool AreEqual(Document a, Document b) const override;
};

class SortByPrice : public ISort {
public:
    SortByPrice() = default;

    ~SortByPrice() override = default;

    bool IsGreater(Document a, Document b) const override;

    bool AreEqual(Document a, Document b) const override;
};

class SortByDiscount: public ISort {
public:
    SortByDiscount() = default;

    ~SortByDiscount() override = default;

    bool IsGreater(Document a, Document b) const override;

    bool AreEqual(Document a, Document b) const override;
};

class SortByPriceWithDiscount : public ISort {
public:
    SortByPriceWithDiscount() = default;

    ~SortByPriceWithDiscount() override = default;

    bool IsGreater(Document a, Document b) const override;

    bool AreEqual(Document a, Document b) const override;
};

class SortByDistance : public ISort {
public:
    SortByDistance(double lat, double lng);

    ~SortByDistance() override = default;

    bool IsGreater(Document a, Document b) const override;

    bool AreEqual(Document a, Document b) const override;

private:
    double lat_;
    double lng_;
};

