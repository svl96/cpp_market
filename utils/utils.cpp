//
// Created by vlaser on 4/16/19.
//

#include <sstream>
#include <iomanip>
#include <cmath>
#include "utils.h"

#define PI 3.14159265

std::string toLowerCase(const std::string input) {
    std::string res;
    for (char it : input)
        res.push_back(static_cast<char>(tolower(it)));
    return res;
}

std::string DoubleToString(double num, int precision) {
    std::ostringstream oss;
    oss << std::fixed << std::setprecision(precision) << num;
    return oss.str();
}

double DegreeToRadian(double degree) {
    return degree * PI / 180.0;
}

double GetDistBetweenPoints(double lat_first, double lng_first,
        double lat_sec, double lng_sec) {

    if (std::abs(lat_first - lat_sec) < 0.00001 && std::abs(lng_first - lng_sec) < 0.00001) {
        return 0.0;
    }

    size_t R = 6371;
    double d_lat = DegreeToRadian(lat_sec - lat_first);
    double d_lng = DegreeToRadian(lng_sec - lng_first);

    double a = std::sin(d_lat / 2) * std::sin(d_lat / 2) + \
                std::cos(DegreeToRadian(lat_first)) * std::cos(DegreeToRadian(lat_sec)) * \
                std::sin(d_lng / 2) * std::sin(d_lng / 2);

    double c = 2 * std::atan2(std::sqrt(a), std::sqrt(1 - a));
    double d = R * c;

    return d;
}