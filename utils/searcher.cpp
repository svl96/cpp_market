//
// Created by vlaser on 03-Apr-19.
//

#include "searcher.h"
#include <algorithm>


Searcher::Searcher(const std::vector<Document> &docs) : docs_(docs) {}

Searcher &Searcher::Filter(const IFilter *filter) {
    std::vector<Document> tmp;
    for (const auto &doc: docs_) {
        if (filter->Check(doc)) {
            tmp.push_back(doc);
        }
    }
    docs_ = std::move(tmp);
    delete filter;
    return *this;
}

Searcher &Searcher::Sort(const ISort *sort, bool desc) {

    auto comparator = [sort, desc](Document a, Document b)
            { return desc == sort->IsGreater(a, b); };
    std::sort(docs_.begin(), docs_.end(), comparator);
    delete sort;
    return *this;
}

Searcher &Searcher::Sort(std::vector<ISort*> sorters, bool desc) {

    auto comparator = [sorters, desc](Document a, Document b)
    {
        for (auto sort : sorters) {
            if (sort->AreEqual(a, b)) {
                continue;
            }
            return desc == sort->IsGreater(a, b);
        }
        return false;
    };
    std::sort(docs_.begin(), docs_.end(), comparator);

    for (ISort *sorter: sorters) {
        delete sorter;
    }
    sorters.clear();
    return *this;
}

std::vector<std::string> Searcher::Serialize(const ISerializer *serializer) {
    std::vector<std::string> result;
    result.reserve(docs_.size());
    for (const auto &doc : docs_) {
        result.push_back(serializer->Serialize(doc));
    }
    delete serializer;
    return result;
}
