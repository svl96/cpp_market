#include <string>
#include <vector>
#include "../entity/entities.h"

#pragma once

class IFilter {
public:
    IFilter() = default;
    virtual ~IFilter() = default;

    virtual bool Check(const Document &doc) const = 0;
};

class TitleFilter : public IFilter {
public:
    explicit TitleFilter(const std::string& title);

    ~TitleFilter() override = default;

    bool Check(const Document &doc) const override;

private:
    std::string title_;
};

class PriceFilter : public IFilter {
public:
    explicit PriceFilter(size_t max);

    PriceFilter(size_t min, size_t max);

    ~PriceFilter() override = default;

    bool Check(const Document &doc) const override;

private:
    size_t min_;
    size_t max_;
};

class LocationFilter : public IFilter {
public:
    LocationFilter(double lat, double lng, double dist_km);

    ~LocationFilter() override = default;

    bool Check(const Document &doc) const override;

private:
    double lat_;
    double lng_;
    double dist_km_;
};

class ShopFilter : public IFilter {
public:
    explicit ShopFilter(const std::vector<std::string>& shops);

    ~ShopFilter() override = default;

    bool Check(const Document &doc) const override;

private:
    std::vector<std::string> shops_;
};

class PriceWithDiscountFilter : public IFilter {
public:
    explicit PriceWithDiscountFilter(size_t max);

    ~PriceWithDiscountFilter() override = default;

    PriceWithDiscountFilter(size_t min, size_t max);

    bool Check(const Document &doc) const override;

private:
    size_t min_;
    size_t max_;
};
