//
// Created by vlaser on 4/16/19.
//
#include <string>
#pragma once

std::string toLowerCase(std::string input);

std::string DoubleToString(double num, int precision=2);

double DegreeToRadian(double degree);

double GetDistBetweenPoints(double lat_first, double lng_first,
                            double lat_sec, double lng_sec);
