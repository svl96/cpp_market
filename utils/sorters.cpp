//
// Created by vlaser on 4/3/19.
//

#include "sorters.h"
#include "utils.h"


bool SortByTitle::IsGreater(Document a, Document b) const {
    return a.title > b.title;
}

bool SortByTitle::AreEqual(Document a, Document b) const {
    return a.title == b.title;
}

bool SortByPrice::IsGreater(Document a, Document b) const {
    return a.price > b.price;
}

bool SortByPrice::AreEqual(Document a, Document b) const {
    return a.price == b.price;
}

bool SortByDiscount::IsGreater(Document a, Document b) const {
    return a.discount > b.discount;
}

bool SortByDiscount::AreEqual(Document a, Document b) const {
    return a.discount == b.discount;
}

bool SortByPriceWithDiscount::IsGreater(Document a, Document b) const {
    auto a_price_disc = static_cast<size_t>(a.price * ((double)(100 - a.discount) / 100));
    auto b_price_disc = static_cast<size_t>(b.price * ((double)(100 - b.discount) / 100));
    return a_price_disc > b_price_disc;
}

bool SortByPriceWithDiscount::AreEqual(Document a, Document b) const {
    auto a_price_disc = static_cast<size_t>(a.price * ((double)(100 - a.discount) / 100));
    auto b_price_disc = static_cast<size_t>(b.price * ((double)(100 - b.discount) / 100));
    return a_price_disc == b_price_disc;
}

SortByDistance::SortByDistance(double lat, double lng)
        : lat_(lat), lng_(lng) {}

bool SortByDistance::IsGreater(Document a, Document b) const {
    double dist_a = GetDistBetweenPoints(lat_, lng_, a.latitude, a.longitude);
    double dist_b = GetDistBetweenPoints(lat_, lng_, b.latitude, b.longitude);

    return dist_a > dist_b;
}

bool SortByDistance::AreEqual(Document a, Document b) const {
    double dist_a = GetDistBetweenPoints(lat_, lng_, a.latitude, a.longitude);
    double dist_b = GetDistBetweenPoints(lat_, lng_, b.latitude, b.longitude);

    return dist_a == dist_b;
}
