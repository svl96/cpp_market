#pragma once

#include <string>
#include <vector>
#include "../entity/entities.h"
#include "filters.h"
#include "sorters.h"
#include "serializers.h"

class Searcher {
public:
    explicit Searcher(const std::vector<Document> &docs);

    Searcher(const Searcher &other) = default;

    Searcher& Filter(const IFilter* filter);
    Searcher& Sort(const ISort* sort, bool desc=false);
    Searcher& Sort(std::vector<ISort*> sorters, bool desc=false);
    std::vector<std::string> Serialize(const ISerializer* serializer);

private:
    std::vector<Document> docs_;
};
