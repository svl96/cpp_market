#include <utility>
#include <string>

#pragma once

struct Document {
    Document(const std::string& title, size_t price, short discount,
             double latitude, double longitude, std::string shop) :
             title(title), price(price), discount(discount),
             latitude(latitude), longitude(longitude), shop(std::move(shop)) {
        highlight_title = this->title;
    }

    std::string title;
    size_t price;
    short discount;
    double latitude;
    double longitude;
    std::string shop;


    mutable std::string highlight_title;
};
