//
// Created by vlaser on 4/16/19.
//

#include "../entry_point.h"
//
#include "gtest/gtest.h"
#include "gmock/gmock.h"

using ::testing::ElementsAre;
using ::testing::UnorderedElementsAre;

namespace {
    class SimpleTests : public ::testing::Test {
    protected:
        Searcher* searcher;

        void SetUp() override {
            Document iphone7plus("Iphone 7 plus", 50000, 20, 76.2f, -25.1f, "eldorado");
            Document iphone7minus("Iphone 7 minus", 10000, 95, 1.2f, 20.1f, "mvideo");
            Document galaxyS7("Samsung galaxy s7", 14000, 15, 21.2f, -11.8f, "mvideo");
            Document lumiaSX("Nokia Lumia SX", 22000, 45, -23.2f, -42.1f, "svyaznoy syzran");
            std::vector<Document> docs = {
                    iphone7plus,
                    iphone7minus,
                    galaxyS7,
                    lumiaSX
            };
            searcher = new Searcher(docs);
        }

        void TearDown() override {
            delete searcher;
        }
    };

    TEST_F(SimpleTests, SimpleFilter) {
        auto result = searcher
                ->Filter(new PriceFilter(14000))
                .Serialize(new CSVSerializer({DOCFIELD::TITLE, DOCFIELD::PRICE}));

        ASSERT_THAT(result, UnorderedElementsAre(
                "Iphone 7 minus,10000",
                "Samsung galaxy s7,14000"
        ));
    }

    TEST_F(SimpleTests, CaseSence) {
        auto result = searcher
                ->Filter(new TitleFilter("iphone"))
                .Serialize(new CSVSerializer({DOCFIELD::TITLE, DOCFIELD::PRICE}));

        ASSERT_THAT(result, UnorderedElementsAre(
                "Iphone 7 minus,10000",
                "Iphone 7 plus,50000"
        ));
    }

    TEST_F(SimpleTests, FilterSortAndSerialize) {
        auto result = searcher
                ->Filter(new TitleFilter("phone"))
                .Sort(new SortByPrice())
                .Serialize(new CSVSerializer({DOCFIELD::PRICE, DOCFIELD::TITLE}));

        ASSERT_THAT(result, ElementsAre(
                "10000,Iphone 7 minus",
                "50000,Iphone 7 plus"
        ));
    }


    TEST_F(SimpleTests, SimplePriceFilterSortTitle) {
        auto result = searcher
                ->Filter(new PriceFilter(11000, 23000))
                .Sort(new SortByTitle())
                .Serialize(new CSVSerializer({DOCFIELD::TITLE, DOCFIELD::PRICE}));

        ASSERT_THAT(result, ElementsAre(
                "Nokia Lumia SX,22000",
                "Samsung galaxy s7,14000"
        ));
    }

    TEST_F(SimpleTests, ShopFilterDiscountSort) {
        auto result = searcher
                ->Filter(new ShopFilter({"eldorado", "svyaznoy syzran"}))
                .Sort(new SortByDiscount(), true)
                .Serialize(new TSVSerializer({DOCFIELD::SHOP, DOCFIELD::DISCOUNT}));

        ASSERT_THAT(result, ElementsAre(
                "svyaznoy syzran\t45",
                "eldorado\t20"
        ));
    }

    TEST_F(SimpleTests, DistanceFilterPriceWithDicsSort) {
        auto result = searcher
                ->Filter(new LocationFilter(21.2f, -11.8f, 10.0))
                .Sort(new SortByPriceWithDiscount(), true)
                .Serialize(new TSVSerializer({DOCFIELD::SHOP,
                                              DOCFIELD::LATITUDE,
                                              DOCFIELD::LONGITUDE}));

        ASSERT_THAT(result, ElementsAre(
                "mvideo\t21.20\t-11.80"
        ));
    }


    TEST_F(SimpleTests, MultipleSort) {
        auto result = searcher
                ->Filter(new TitleFilter("7"))
                .Sort({new SortByTitle(), new SortByPrice()})
                .Serialize(new HighlightTitleCSVSerializer({DOCFIELD::TITLE}));

        ASSERT_THAT(result, ElementsAre(
                "Iphone *7* minus",
                "Iphone *7* plus",
                "Samsung galaxy s*7*"
        ));
    }

    TEST_F(SimpleTests, DistSort) {
        auto result = searcher
                ->Filter(new TitleFilter("phone 7"))
                .Sort(new SortByDistance(1.2f, 20.1f))
                .Serialize(new HighlightTitleCSVSerializer({DOCFIELD::TITLE}));

        ASSERT_THAT(result, ElementsAre(
                "I*phone 7* minus",
                "I*phone 7* plus"));
    }

    TEST_F(SimpleTests, MultSort) {
        Document iphone7eldorado("Iphone 7 plus", 50000, 20, 76.2f, -25.1f, "eldorado");
        Document iphone7mvideo("Iphone 7 plus", 40000, 20, 71.2f, -24.1f, "mvideo");
        Document iphone7mm("Iphone 7 plus", 30000, 20, 64.2f, -123.1f, "media market");
        Document iphone7dns("Iphone 7 plus", 20000, 20, 34.2f, -12.1f, "dns");
        std::vector<Document> docs = {
                iphone7dns,
                iphone7mm,
                iphone7eldorado,
                iphone7mvideo
        };
        Searcher searcher(docs);

        auto result = searcher
                .Filter(new PriceWithDiscountFilter(40000))
                .Sort({new SortByTitle(), new SortByPriceWithDiscount(), new SortByDiscount()})
                .Serialize(new HighlightTitleCSVSerializer({DOCFIELD::TITLE, DOCFIELD::SHOP}));

        ASSERT_THAT(result, ElementsAre(
                "Iphone 7 plus,dns",
                "Iphone 7 plus,media market",
                "Iphone 7 plus,mvideo",
                "Iphone 7 plus,eldorado"));
    }

    TEST_F(SimpleTests, MultSortDist) {
        Document iphone7eldorado("Iphone 7 plus", 50000, 20, 76.2f, -25.1f, "eldorado");
        Document iphone7mvideo("Iphone 7 plus", 40000, 20, 71.2f, -24.1f, "mvideo");
        Document iphone7mm("Iphone 7 plus", 30000, 20, 71.2f, -20.1f, "media market");
        Document iphone7dns("Iphone 7 plus", 20000, 20, 71.2f, -18.1f, "dns");
        std::vector<Document> docs = {
                iphone7dns,
                iphone7mm,
                iphone7eldorado,
                iphone7mvideo
        };
        Searcher searcher(docs);

        auto result = searcher
                .Filter(new PriceWithDiscountFilter(10, 40000))
                .Sort({new SortByTitle(), new SortByDistance(76.2f, -25.1f)})
                .Serialize(new HighlightTitleCSVSerializer({DOCFIELD::SHOP}));

        ASSERT_THAT(result, ElementsAre("eldorado", "mvideo",  "media market", "dns"));
    }


    TEST_F(SimpleTests, MultSortDist2) {
        Document iphone7eldorado("Iphone 7 plus", 50, 20, 76.2f, -25.1f, "eldorado");
        Document iphone7mvideo("Iphone 7 plus", 400, 20, 71.2f, -25.1f, "mvideo");
        Document iphone7mm("Iphone 7 plus", 3000, 20, 71.2f, -25.1f, "media market");
        Document iphone7dns("Iphone 7 plus", 20000, 20, 71.2f, -25.1f, "dns");
        std::vector<Document> docs = {
                iphone7dns,
                iphone7mm,
                iphone7eldorado,
                iphone7mvideo
        };
        Searcher searcher(docs);


        auto result = searcher
                .Filter(new PriceWithDiscountFilter(10, 40000))
                .Sort({new SortByTitle(), new SortByDistance(76.2f, -25.1f), new SortByPrice()})
                .Serialize(new HighlightTitleCSVSerializer({DOCFIELD::SHOP}));

        ASSERT_THAT(result, ElementsAre("eldorado", "mvideo",  "media market", "dns"));
    }


    TEST_F(SimpleTests, MultipleHeaderFilter) {
        Document phone1("Iphone 7 plus", 50, 20, 76.2f, -25.1f, "eldorado");
        Document phone2("Iphone 7 minus", 400, 20, 71.2f, -25.1f, "mvideo");
        Document phone3("Huawei", 3000, 20, 71.2f, -25.1f, "media market");
        Document phone4("Motorola", 20000, 20, 71.2f, -25.1f, "dns");
        std::vector<Document> docs = {
                phone4,
                phone3,
                phone1,
                phone2
        };
        Searcher searcher(docs);

        auto result = searcher
                .Filter(new TitleFilter("PhoNe"))
                .Filter(new TitleFilter("us"))
                .Serialize(new HighlightTitleCSVSerializer({DOCFIELD::TITLE}));

        ASSERT_THAT(result, UnorderedElementsAre("I*phone* 7 pl*us*", "I*phone* 7 min*us*"));
    }


    TEST_F(SimpleTests, DistTest) {
        Document phone1("Iphone 7 plus", 50, 20, 59.939f, 30.31f, "kazan");
        Document phone2("Iphone 7 minus", 400, 20, 55.796f, 49.108f, "saint-petersburg");
        Document phone3("Huawei", 3000, 20, 55.75f, 37.617f, "moscow");
        std::vector<Document> docs = {
                phone3,
                phone1,
                phone2
        };
        Searcher searcher(docs);

        auto result = searcher
                .Filter(new LocationFilter(55.796f, 49.108f, 800))
                .Serialize(new CSVSerializer({DOCFIELD::SHOP}));

        ASSERT_THAT(result, UnorderedElementsAre("saint-petersburg", "moscow"));
    }



}